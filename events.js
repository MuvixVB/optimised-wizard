var sectionError = false;

$(window).on('load', function ()
{
    $('.fixed-height').niceScroll();
});

$(document).on('click', '[data-section]', function (e)
{
    e.preventDefault();

    var t = $(this);
    if (!t.hasClass('disabled'))
    {
        var s = t.data('section');

        $(document).find('[data-menu="'+s+'"]').removeClass('closed')
            .siblings().addClass('closed');
        $(document).find('[data-titles="'+s+'"]').removeClass('closed')
            .siblings().addClass('closed');
        $(document).find('[data-form="'+s+'"]').removeClass('closed')
            .siblings().addClass('closed');
    }
    return false
});

$(document).on('click', '[data-show-step]', function (e)
{
    e.preventDefault();

    var t = $(this);
    if (!t.hasClass('disabled'))
    {
        $('#editor_buttons, #editor_menus').addClass('closed');
        $('#editor_titles, #editor_forms').removeClass('closed');
        var b = t.closest('ol').data('menu');
        var s = t.data('show-step');

        $(document).find('[data-form="'+b+'"] [data-title="'+s+'"]').removeClass('closed')
            .siblings().addClass('closed');
        $(document).find('[data-form="'+b+'"] [data-step="'+s+'"]').removeClass('closed').addClass('current-step')
            .siblings().addClass('closed').removeClass('current-step');
    }
    return false
});

$(document).on('click', '.prev_step, .next_step', function(e)
{
    sectionError = false;
    e.preventDefault();

    var t = $(this);
    var section = t.closest('[data-step]');
    sectionError = checkStep(section);

    if (t.hasClass('disabled'))
    {
        return false;
    }

    completeStep(section);
    checkForSizing();
    if ( !sectionError )
    {
        step(t);
    }
});

$(document).on('click', '.switch-to-menu', function (e)
{
    var error = false;
    e.preventDefault();
    var current = $('.current-step');
    error = checkStep(current);
    completeStep(current, !error);
    goMenu();
});

$(document).on('click', '.switch-to-next', function (e)
{
    e.preventDefault();
    var current = $('.current-step');
    var next = current.find('.next_step');
    if (next.length > 0)
    {
        next.trigger('click');
    }
    else
    {
        var last = current.find('.last_step');
        if (last.length > 0)
        {
            last.trigger('click');
        }
    }
});

$(document).on('click', '#btn_personalise', function (e)
{
    e.preventDefault();
    var t = $(this);
    if (t.hasClass('disabled'))
    {
        showPersonalizationNotify(true);
        setTimeout(
            showPersonalizationNotify,
            3000
        );
        return false;
    }

    $(document).find('#head_personalise').trigger('click');
});

$(document).on('mouseover','#head_personalise.disabled',function ()
{
    showPersonalizationNotify(true, 'head');
});

$(document).on('mouseover','#btn_personalise.disabled',function ()
{
    showPersonalizationNotify(true);
});

$(document).on('mouseout', '#btn_personalise.disabled, #head_personalise.disabled', showPersonalizationNotify);

$(document).on('click', '.check-patch', function ()
{
    isChecked($(this));
});

$(document).on('click', '.p-back', function ()
{
    $('.p-back').addClass('small');
});

function checkForBuy ()
{
    var step = $(document).find('[data-form="personalise"] [data-step="6"]');
    var buy = $('#buy');
    if (!checkStep(step))
    {
        buy.removeClass('disabled');
    }
    else
    {
        buy.addClass('disabled');
    }
}

function checkForSizing ()
{
    var steps = $(document).find('[data-form="personalise"] [data-step]');
    var error = false;
    var tmp_err = false;
    steps.each(function ()
    {
        var t = $(this);
        if (parseInt(t.data('step')) < steps.length)
        {
            error = checkStep(t);
            if (error)
            {
                tmp_err = true;
            }
        }
    });
    var btns = $('#sizing, #sizing-btn');
    if (tmp_err)
    {
        btns.addClass('disabled');
    }
    else
    {
        btns.removeClass('disabled');
    }
}

function completeStep ( element, bool )
{
    var type = element.closest('[data-form]').data('form');
    var index = element.data('step');
    var sizing = $(document).find('#sizing');
    var sizing_btn = $(document).find('#sizing-btn');
    if (!_is(index))
    {
        index = element.closest('[data-step]').data('step');
    }
    var menu = $('[data-menu="'+type+'"]');
    var step = menu.find('[data-show-step="'+index+'"]');
    if (bool === false)
    {
        allowPersonalisation(false);
        step.removeClass('completed');
        if (element.closest('[data-form]').data('form') != 'materials')
        {
            sizing.addClass('disabled');
            sizing_btn.addClass('disabled');
            allowPersonalisation(true);
            return false;
        }
        return false;
    }
    if (element.closest('[data-form]').data('form') != 'materials')
    {
        return false;
    }
    step.addClass('completed');
    var items = menu.find('li');
    var completed = menu.find('li.completed');
    bool = (items.length == completed.length);
    allowPersonalisation(bool);
}

function step ( button )
{
    var section = button.closest('[data-step]');
    var index = parseInt(section.data('step'));
    var form = button.closest('[data-form]');

    if (button.hasClass('prev_step'))
    {
        if (index == 1)
        {
            goMenu();
        }
        else
        {
            section.prev('[data-step]').removeClass('closed').addClass('current-step')
                .siblings().addClass('closed').removeClass('current-step');
        }
    }
    if (button.hasClass('next_step'))
    {
        section.next('[data-step]').removeClass('closed').addClass('current-step')
            .siblings().addClass('closed').removeClass('current-step');
    }
}

function goMenu ()
{
    $('#editor_buttons, #editor_menus').removeClass('closed');
    $('#editor_titles, #editor_forms').addClass('closed');
    $('.current-step').removeClass('current-step');
}

function isChecked ( label )
{
    label.addClass('checked').siblings().removeClass('checked');
}

function enableList ()
{
    $('#menu_materials li').removeClass('disabled');
}

function showColorList ( span )
{
    var list = span.closest('section').find('.color-list');
    if (list.hasClass('closed'))
    {
        list.removeClass('closed');
        var top = span.position().top;
        list.css('top', top-list.height()/2);
    }
    else
    {
        list.addClass('closed');
    }
}

function setColor ( input )
{
    var span = input.closest('section').find('.color-icon');
    span.addClass('checked').css('background-color', input.val());
    input.closest('.color-list').addClass('closed');
}

function openSub ( input, double, row )
{
    var article, val;
    if (!double)
    {
        article = input.closest('.row-field');
        if (article.length < 1)
        {
            article = input.closest('article');
        }
        val = input.val();
        article.children('[data-f-val]').addClass('closed');
        article.children('.sub-field[data-f-val*="'+val+'"]').removeClass('closed');
        return false;
    }
    article = input.closest('section.sub-field');
    val = input.val();
    article.children('[data-f-val]').addClass('closed');
    article.children('.sub-field[data-f-val*="'+val+'"]').removeClass('closed');
}

function openRow ( field, row )
{
    if (field.val().length > 0)
    {
        row.removeClass('disabled');
    }
    else
    {
        row.addClass('disabled');
    }
}

function setJacket ( material )
{
    var wool = $(document).find('[data-material="wool"]');
    var leather = $(document).find('[data-material="leather"]');
    switch (material)
    {
        case 'leather':
            wool.addClass('closed');
            leather.trigger('click');
            leather.each(function ()
            {
                var t = $(this);
                var error = checkStep( t.closest('[data-step]') );
                if (error)
                {
                    completeStep(t, false);
                }
            });
        break;
        case 'wool':
            wool.removeClass('closed');
        break;
    }
}

function checkStep ( section )
{
    var error = false;
    var errors = section.find('.f-error');

    errors.each(function ()
    {
        var isErr = false;
        var er = $(this);
        if (er.closest('.sub-field').length > 0 && er.closest('.sub-field').hasClass('closed')
            || (er.hasClass('disabled') || er.closest('.row-field').hasClass('disabled')) )
        {
            return true;
        }
        er.addClass('closed');
        var cf = er.data('check-for');
        if (_is(cf))
        {
            var field = $(document).find('input[name="'+cf+'"]');
            if (field.length > 0)
            {
                var ftype = field.attr('type');
                switch(ftype)
                {
                    case 'text':
                        if (field.val().length < 1) isErr = true;
                        break;
                    case 'radio': case 'checkbox':
                    if ($(document).find('input[name="'+cf+'"]:checked').length < 1) isErr = true;
                    break;
                }
            }
            else
            {
                field = $(document).find('select[name="'+cf+'"]');
                if (field.val() == "")
                {
                    isErr = true;
                }
            }
            if (isErr)
            {
                er.removeClass('closed');
                error = true;
            }
        }
    });

    return error;
}

function allowPersonalisation ( bool )
{
    var button = $('[data-section="personalise"]');
    var last_btn = $('#btn_personalise');
    if (bool === false)
    {
        button.addClass('disabled');
        last_btn.addClass('disabled');
        return false;
    }

    button.removeClass('disabled');
    last_btn.removeClass('disabled');
    return true;
}

function showPersonalizationNotify ( show, attr)
{
    var notify = $('#personalise_notify');
    if (!attr) attr = '';
    if (!show || typeof(show) == 'object')
    {
        notify.addClass('closed').removeClass('head');
        return false;
    }
    notify.removeClass('closed').addClass(attr);
}

function enableErrorFields ( field )
{
    var name = field.attr('name');
    var fields = $('[data-check-field="'+name+'"]');
    fields.each(function ()
    {
        var t = $(this);
        var e = $('.f-error[data-check-for="'+ t.attr('name')+'"]');
        if (field.val().length < 1)
        {
            e.addClass('disabled closed');
            return true;
        }
        e.removeClass('disabled');
    });
}

function disableRow ( radio, row )
{
    var article = radio.closest('article');
    var sub = article.children('.sub-field[data-f-val="'+radio.val()+'"]');
    var label = sub.find('label').first();
    var input = label.children('input');
    var type = input.attr('type');
    var disable = false;
    switch (type)
    {
        case 'text':
            if (input.val() < 1)
            {
                disable = true;
            }
            break;
        case 'radio':
            if (sub.find('input:checked').length < 1)
            {
                disable = true;
            }
            break;
    }
    if (disable)
    {
        row.addClass('disabled');
    }
    else
    {
        row.removeClass('disabled');
    }
}

function enableRow ( row )
{
    row.removeClass('disabled');
}

function dublicate ( field, isColor, isPatch )
{
    var name = field.attr('name');
    if (field.attr('type') == 'text' || field.get(0).nodeName == 'SELECT')
    {
        var fields = $('[name="'+name+'"]');
        fields.val( field.val() );
        fields.each(function ()
        {
            var t = $(this);
            var a = '';
            if (t != field)
            {
                if (a = t.attr('onclick'))
                {
                    t.attr('onclick', a.replace('dublicate($(this))', ''));
                    t.trigger('click');
                    t.attr('onclick', a);
                }
                if (a = t.attr('onchange'))
                {
                    t.attr('onchange', a.replace('dublicate($(this))', ''));
                    t.trigger('change');
                    t.attr('onchange', a);
                }
                if (a = t.attr('onkeyup'))
                {
                    t.attr('onkeyup', a.replace('dublicate($(this))', ''));
                    t.trigger('keyup');
                    t.attr('onkeyup', a);
                }
            }
            if (t.get(0).nodeName == "SELECT")
            {
                t.find('option[value="'+field.val()+'"]').trigger('click');
            }
        });
    }

    if (isColor)
    {
        $('input[name="'+name+'"]').closest('.sub-field').find('span.color-icon').css(
            'background', field.val()
        ).addClass('checked');
    }

    if (isPatch)
    {
        $('input[name="'+name+'"][value="'+field.val()+'"]').closest('label').addClass('checked')
            .siblings().removeClass('checked');
    }
}

//$(document).on("keypress", "[data-length], [data-type], [data-upper]", function (e)
//{
//    var x = e.which || e.keyCode;
//
//    if (x != 8 ) //backspace
//    {
//        e.preventDefault();
//        var key = String.fromCharCode(e.which);
//        var t = $(this);
//        var value, length, type, upper;
//
//        value = t.val();
//        length = (t.data("length")) ? t.data("length") : 20 ;
//        type = (t.data("type")) ? t.data("type") : "multi" ;
//        upper = (t.data("upper")) ? t.data("upper") : "lower";
//
//        t.val( js_validate(value + key, length, type, upper) );
//    }
//});

var js_validate = function (value, length, type, uppercase )
{
    var out = value;

    if (uppercase == "uppercase")
    {
        out = out.toUpperCase();
    }

    if (uppercase == "capitalize")
    {
        out = out.charAt(0).toUpperCase() + out.substr(1, length).toLowerCase();
    }



    if (type)
    {
        switch (type)
        {
            default:
                out = out.replace(/[^A-Za-z0-9]+/g, "");
                break;
            case "text":
                out = out.replace(/[^a-zA-Z]+/g, "");
                break;
            case "number":
                out = out.replace(/[^0-9]+/g, "");
                break;
            case "decimal":
                out = out.replace(/[^0-9.,]+/g, "");
                break;
            case "all" :
                out = out.replace(/[^A-Za-z0-9 .,]+/g, "");
                break;
            case "firstNumber":
                if ( isNumeric( value.charAt(0) ) )
                {
                    out = out.replace(/[^A-Za-z0-9]+/g, "");
                    out = out.substr(0, 2);
                }
                else
                {
                    out = out.replace(/[^a-zA-Z]+/g, "");
                }
                break;
        }
    }

    if (length)
    {
        out = out.substr(0, length);
    }

    return out;
};

$(document).on('keypress', '[data-v-type]', function (e)
{
    var x = e.which || e.keyCode;
    var codes = [8, 35, 36, 37, 38, 39, 40, 46];
    if (codes.indexOf(x) < 0)
    {
        e.preventDefault();
        validate($(this), e);
    }
});

function validate ( t, e )
{
    var type = t.data('v-type');
    var l = t.data('v-length');
    var s = t.get(0).selectionStart;
    var c = t.data('case');
    var str = t.val();
    var key = String.fromCharCode(e.which);
    if (str.length < l)
    {
        str = str.substr(0, s) + key + str.substr(s);
        s++;
    }
    switch (type)
    {
        default:
            str = str.replace(/[^A-Za-z0-9]+/g, "");
            break;
        case 'text':
            str = str.replace(/[^A-Za-z ]+/g, "");
            break;
        case 'left_chest':
            if (str.length > 0)
            {
                if (str.charAt(0) >=0 && str.charAt(0) <= 9)
                {
                    l = 2;
                    str = str.replace(/[^A-Za-z0-9]+/g, "");
                }
                else
                {
                    str = str.replace(/[^A-Za-z]+/g, "");
                }
            }
            break;
        case 'lines':
            str = str.replace(/[^A-Za-z0-9 -]+/g, "");
            break;
    }

    switch (c)
    {
        case 'upper':
            str = str.toUpperCase();
            break;
        case 'cap':

            break;
        case 'cap_each':
            str = str.replace(/\w\S*/g, function(txt)
            {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
            break;
    }
    str = str.substr(0, l);
    t.val(str);
    t.setCursorPosition(s);
}

$.fn.serializeObject = function(){
    var self = this,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };

    this.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    this.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(this).serializeArray(), function(){
        if(!patterns.validate.test(this.name)){
            return;
        }
        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;
        while((k = keys.pop()) !== undefined){
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }
        json = $.extend(true, json, merge);
    });
    return json;
};

$.fn.setCursorPosition = function(pos) {
    this.each(function(index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};