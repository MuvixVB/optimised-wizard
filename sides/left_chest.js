var LeftChest = function ()
{
    var x = this;
    this.text = '';
    this.font = 'AthleticBlock';
    this.color = '';
    this.outline = '';
    this.patch = new Image();

    this.reload = function ()
    {
        ctx.textAlign = "start";
        if (_is(a = jd.personalisation) && _is(b = a.left_chest))
        {
            x.text = b.text;
            x.font = (b.font != '') ? b.font : defFont;
            x.color = (_is(b.color)) ? b.color : defColor;
            x.outline = (_is(b.outline)) ? b.outline : defColor;
        }

        var b = (jd.personalisation) ? jd.personalisation.left_chest : {type:''};

        x.print(b);
    };

    this.print = function (b)
    {
        if (b.type == 'patch')
        {
            if (_is(b.patch))
            {
                if (x.patch.src != b.patch)
                {
                    x.patch.src = b.patch;
                    x.patch.onload = x.drawPatch();
                }
                else
                {
                    x.drawPatch();
                }
            }
        }
        else
        {
            if (x.text.length)
            {
                if (jd.personalisation.left_chest.color_type == 1)
                {
                    x.drawLetters(true);
                }
                x.drawLetters(false);
            }
        }
    };

    this.drawPatch = function ()
    {
        var custom = wiz.getPatchPrintSize(x.patch, 64);
        ctx.drawImage(x.patch, 30 + custom.x, -110 + custom.y, custom.width, custom.height);
    };

    this.drawLetters = function (type)
    {
        var size = 90;
        var top, left = 40;
        if (x.text.length == 2) size = 60;
        if (x.text.length == 3) size = 40;

        switch(x.text.length)
        {
            default:
                size = 100; top = -40;
                if (x.font == "script")
                {
                    size = 90; top = -46; left = 30;
                }
                if (x.font == "oldenglish") { size = 260; }
                break;
            case 2:
                size = 60;  top = -84;
                if (x.font == "oldenglish") { size = 150; }
                break;
            case 3:
                size = 44;  top = -90;
                if (x.font == "oldenglish") { size = 100; }
                break;
        }
        ctx.font = size + 'px ' + fonts[x.font];
        ctx.strokeStyle = x.outline;
        ctx.fillStyle = x.color;
        var ff = 0;
        for (var i=0; i<x.text.length; i++)
        {
            var l = 0, t = 0;
            switch (i)
            {
                default:
                    l = 0;  t = 0;
                    if (x.text.length == 2)
                    {
                        t = 16;
                    }
                    break;
                case 1:
                    l = 20; t = 39;
                    if (x.font == "problock") { l = 16; }
                    if (x.font == "script") { l = 8; }
                    if (x.font == "oldenglish") { t = 34; }
                    if (isNumeric(x.text.charAt(0)))
                    {
                        t = 0;
                        l = 30;
                    }
                    if (x.text.length<3)
                    {
                        break;
                    }
                case 2:
                    l = 16; t = 30;
                    if (x.font == "problock") { l = 12; t = 28; }
                    if (x.font == "script") { l = 10; t = 25; }
                    if (x.font == "oldenglish") { l = 13; t = 24; }
                    break;
            }

            left += l;
            top += t;
            if (type)
            {
                ctx.strokeText( x.text[i], left + ff, top );
            }
            else
            {
                ctx.fillText(x.text[i], left + ff, top);
            }
        }
    };
};